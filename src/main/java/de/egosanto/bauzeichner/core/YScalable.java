package de.egosanto.bauzeichner.core;

public interface YScalable {
    public void scaleY(float yScaleFactor);
}
