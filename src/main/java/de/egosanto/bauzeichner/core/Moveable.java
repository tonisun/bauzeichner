package de.egosanto.bauzeichner.core;

public interface Moveable {

    public void move ();
}
