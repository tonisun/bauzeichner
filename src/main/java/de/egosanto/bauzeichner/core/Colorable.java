package de.egosanto.bauzeichner.core;

public interface Colorable {

    public void color ();
}
