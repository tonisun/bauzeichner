package de.egosanto.bauzeichner;

import de.egosanto.bauzeichner.entity.CircularHobbitFrontDoor;
import de.egosanto.bauzeichner.entity.FrontDoor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BauzeichnerApplication {

	public static void main ( String[] args ) {

		SpringApplication.run ( BauzeichnerApplication.class, args );

		System.out.println ( new FrontDoor (123.6f, 212f));

		System.out.println ( new CircularHobbitFrontDoor(212f));
	}

}
