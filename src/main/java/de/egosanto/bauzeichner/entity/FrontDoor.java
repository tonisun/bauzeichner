package de.egosanto.bauzeichner.entity;

import de.egosanto.bauzeichner.core.Colorable;
import de.egosanto.bauzeichner.core.Moveable;
import de.egosanto.bauzeichner.core.YScalable;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class FrontDoor implements YScalable, Colorable, Moveable {

    private Float width;
    private Float height;

    @Override
    public void color() {

    }

    @Override
    public void move() {

    }

    @Override
    public void scaleY(float yScaleFactor) {
        setHeight( getHeight () * yScaleFactor );
    }
}
