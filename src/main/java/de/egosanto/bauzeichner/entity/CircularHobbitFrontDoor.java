package de.egosanto.bauzeichner.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CircularHobbitFrontDoor extends FrontDoor {

    @Setter
    @Getter
    private Float radius;

    @Override
    public void setWidth ( Float width ) {
        setRadius ( width / 2 );
    }

    @Override
    public Float getWidth () {
        return radius * 2;
    }

    @Override
    public void setHeight ( Float height ) {
        setRadius ( height / 2 );
    }

    @Override
    public Float getHeight() {
        return radius * 2;
    }
}
